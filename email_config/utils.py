# -*- coding: utf-8 -*-
from jinja2 import Template
import boto3
from botocore.exceptions import ClientError
from dotenv import load_dotenv

import os

dotenv_path = os.path.join(os.path.dirname(__file__), '../.env')
load_dotenv(dotenv_path, verbose=True)

AWS_REGION = os.getenv("AWS_REGION")
AWS_ACCESS_KEY_ID = os.getenv("AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY = os.getenv("AWS_SECRET_ACCESS_KEY")

aws_client = boto3.client('ses', aws_access_key_id=AWS_ACCESS_KEY_ID,
                      aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                      region_name=AWS_REGION)

daily_email_body = """
<!doctype html>
        <html>
        <body>
        {% block content %}
        <p>Hey {{ result.name }}, </p>
        {% endblock %} 
        <p>Here is the daily auto-generated summary.</p>
        <p><strong>Permitted conversations : {{result.permitted}}</strong></p>
        <p><strong>Conversations used      : {{ result.used_convo }} </strong></p>
            <table>
                <tr>
                    <th>Seesion ID</th>
                    <th>Timestamp</th>
                    <th>Timezone</th>
                </tr>
                {% for data in result.tab %}
                    <tr>
                    <td> {{ data.session }} </td>
                    <td> {{ data.timestamp }} </td>
                    <td> {{ data.timezone }} </td>
                    </tr>
                {% endfor %}
            </table>
            <p>To see today's chat logs in detail - Click here</p>
            <p>Thanks for using Hybrid.Chat!</p>
            <p>Regards.</p>
            <p>Hybrid.Chat Team!</p>
        </body>
        </html>
"""

monthly_email_body = """
<!doctype html>
        <html>
        <body>
        {% block content %}
        <p>Hey {{ result.name }}, </p>
        {% endblock %} 
        <p>Here is the monthly auto-generated summary.</p>
        <p><strong>Permitted conversations : {{result.permitted}}</strong></p>
        <p><strong>Conversations used      : {{result.used_convo}} </strong></p>
            <p>To see chat logs in detail - Click here</p>
            <p>Thanks for using Hybrid.Chat!</p>
            <p>Regards.</p>
            <p>Hybrid.Chat Team!</p>
        </body>
        </html>

"""


def daily_email_send(From, To, email_sub, data):
    t = Template(daily_email_body)
    template_data = t.render(result=data)
    email_response = send_email(From, To, email_sub, template_data)
    print(email_response)


def monthly_email_send(From, To, email_sub, data):
    t = Template(daily_email_body)
    template_data = t.render(result=data)
    email_response = send_email(From, To, email_sub, template_data)
    print(email_response)

def send_email(From, To, Subject, Body_Text):
    try:
        response = aws_client.send_email(
            Destination={
                'ToAddresses': To,
            },
            Message={
                'Body': {
                    # 'Text': {
                    #     'Data': "Body_Text",
                    # },
                    'Html': {
                        'Data': Body_Text
                    }
                },
                'Subject': {
                    'Data': Subject,
                },
            },
            Source=From
        )
    except ClientError as e:
        print("ERROR")
        return e.response['Error']['Message']
    else:
        print("[Message]"),
        print(response['MessageId'])
    return "ok"

# def result():
#     # data = {
#     #     'name' : "Shubham",
#     #     'permitted' : 545,
#     #     'used_convo' : 54,
#     #     }
#     tm = Template(monthly_email_body)
#     tm_body = tm.render(result=data)
#     # email_response = send_email("hello@hybrid.chat", ["shubham.c@solutionbeyond.net"], "email_sub", tt)
#     # print(email_response)
#     # tm_body
#     print(tm_body)
#     return tm

# result()
print("config")
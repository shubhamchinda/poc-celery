from pymongo import MongoClient
from celery import Celery
from celery.schedules import crontab
import datetime
from datetime import date, timedelta
from dotenv import load_dotenv

import os

from email_config.utils import (daily_email_send, monthly_email_send)

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path, verbose=True)

mongoClient = MongoClient('mongodb://mongodb:27017')


FROM_EMAIL = os.getenv("FROM_EMAIL_ID")



app = Celery('taskSchedulers', broker='redis://redis:6379/2')
app.conf.broker_url = 'redis://redis:6379/2'


def get_first_day(dt, d_years=0, d_months=0):
    # d_years, d_months are "deltas" to apply to dt
    y, m = dt.year + d_years, dt.month + d_months
    a, m = divmod(m-1, 12)
    return datetime.datetime(y+a, m+1, 1)

def get_last_day(dt):
    last = datetime.datetime.combine(get_first_day(dt, 0, 1) + timedelta(-1), datetime.time(23, 59, 59, 999999))
    return last


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(
        # crontab(minute=22, hour=23),  #UTC
        60.0,
        daily_summary.s(),
    )
    sender.add_periodic_task(
        # crontab(minute=22, hour=23),  #UTC
        90.0,
        monthly_summary.s(),
    )


@app.task
def daily_summary():
    print("DAILYY")
    user_array = mongoClient.users_db.auth0.find({'bots': {'$exists': 'true', '$ne':[]}})
    subject = "Your Daily Hybrid.Chat Summary "
    for user in user_array:
        permitted_convo = user['max_allowed_conversations']

        timezone = user['time_zone']
        # app.conf.timezone = timezone
        name = "Shubham"
        bots_array = user['bots']
        total_daily_convo = 0
        start_of_today = datetime.datetime.combine(datetime.datetime.today(), datetime.time(0,0,0,0))
        end_of_today = datetime.datetime.combine(datetime.datetime.today(), datetime.time(23, 59, 59, 999999))
        for val in bots_array:
            count_convo = mongoClient[val].histories.distinct('sessionId', {'date': {'$lt': end_of_today, '$gte': start_of_today}})
            total_daily_convo = total_daily_convo + len(count_convo)

        table_data = []
        for id in count_convo:
            temp = {'session': id, 'timestamp': 'few hrs ago', 'timezone' : "Asia"}
            table_data.append(temp)    
        data = {
        'tab' : table_data,
        'name' : "Shubham",
        'permitted' : permitted_convo,
        'used_convo' : total_daily_convo,
        }    
        email_data = {'daily_count' : total_daily_convo, 'permitted_count' : permitted_convo}
        # daily_email_send(FROM_EMAIL,["shubham.c@solutionbeyond.net"], subject, data)
        print(email_data)

@app.task
def monthly_summary():
    user_array = mongoClient.users_db.auth0.find({'bots': {'$exists': 'true', '$ne':[]}})
    subject = "Your monthly Hybrid.Chat Summary "

    for user in user_array:

        permitted_convo = user['max_allowed_conversations']
        bots_array = user['bots']
        today = date.today()
        total_monthly_convo = 0
        name = "Shubham"
    
        start_of_month = get_first_day(today)
        end_of_month = get_last_day(today)

        for val in bots_array:
            count_convo = mongoClient[val].histories.distinct('sessionId', {'date': {'$lt': end_of_month, '$gte': start_of_month}})
            total_monthly_convo = total_monthly_convo + len(count_convo)

        data = {
        'name' : "Shubham",
        'permitted' : permitted_convo,
        'used_convo' : total_daily_convo,
        }
        email_data = {'monthly_count' : total_monthly_convo, 'permitted_count' : permitted_convo}
        # monthly_email_send(FROM_EMAIL,["shubham.c@solutionbeyond.net"], subject, data)

        print(email_data)

# app.conf.beat_schedule = {
#     "demo-task": {
#         "task": "testMongo.test",
#         "schedule": crontab(minute=57, hour=22)
#     }
# }
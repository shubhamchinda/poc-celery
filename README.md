# Installation

Install `virtual env` for creation of virtual environment.

Switch to venv using command `source venv/bin/activate`

Run following commands :

`pip install celery`

# Running celery in virtual env

*   Run beat `celery -A poc beat --loglevel=info`

* Run celery worker log to outputs : `celery -A poc worker --loglevel=info`

* For `testMongo.py` please replace `poc` with the  `testMongo` in above commands.
